"use strict";
exports.__esModule = true;
var http_1 = require("http");
var url_1 = require("url");
var opn_1 = require("opn");
http_1.createServer(function (req, res) {
    var filename = (new url_1.URL(req.url)).pathname.split('.');
    console.log(filename);
    opn_1["default"]('http://localhost:7357/');
}).listen(7357); // get it? 7357 -> TEST
